# Parcel.js

Blazing fast, zero configuration web application bundler.

First install Parcel using Yarn or npm:

Yarn:

```
yarn global add parcel-bundler
```

npm:

```
npm install -g parcel-bundler
```

Now just point it at your entry file:

```
parcel index.html
```

Now open http://localhost:1234/ in your browser.
